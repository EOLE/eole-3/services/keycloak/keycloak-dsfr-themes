<!--
  ~ Copyright 2016 Red Hat, Inc. and/or its affiliates
  ~ and other contributors as indicated by the @author tags.
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~ http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
    <title>Bienvenue sur Keycloak</title>
    <link rel="shortcut icon" href="welcome-content/favicon.ico" type="image/x-icon">
    <link rel="StyleSheet" href="welcome-content/keycloak.css" type="text/css">
</head>

<body>
<div class="wrapper">
    <div class="content">
        <div class="logo">
                <img src="welcome-content/red-hat-logo.png" alt="Red Hat" border="0" />
        </div>
        <h1>Bienvenue sur Keycloak</h1>

        <h3>Votre Keycloak est en cours d'éxecution.</h3>

        <p><a href="http://www.keycloak.org/docs">Documentation</a> | <a href="admin/">Console d'Administration</a> </p>

        <p><a href="http://www.keycloak.org">Le projet Keycloak</a> |
            <a href="https://groups.google.com/forum/#!forum/keycloak-user">Mailing List</a> |
            <a href="https://issues.jboss.org/browse/KEYCLOAK">Signaler une annomalie</a></p>
        <p class="logos"><a href="http://www.jboss.org"><img src="welcome-content/jboss_community.png" alt="JBoss et la communauté JBoss" width="254" height="31" border="0"></a></p>
    </div>
</div>
</body>
</html>
